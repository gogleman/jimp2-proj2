package presenter;

import model.World;
import utils.*;
import view.*;

import java.io.File;


public class WorldPresenter implements Presenter{

    private World wireWorld;
    private WireWorldView wireWorldView;
    private SimulationThread simulationThread;


    public WorldPresenter() {
    }

    public void setWorld(World world) {
        this.wireWorld = world;
    }

    public void setWireWorldView(WireWorldView wireWorldView) {
        this.wireWorldView = wireWorldView;
    }

    public void setSimulationThread(SimulationThread simulationThread) {
        this.simulationThread = simulationThread;
    }

    public void setWWVGAWPParameters(int numberOfGenerations, int animationSpeed){
        if (animationSpeed >10 && animationSpeed < 60000) simulationThread.setAnimationSpeed(animationSpeed);
        if (numberOfGenerations > 0 && numberOfGenerations <10000) simulationThread.setNumberOfGenerations(numberOfGenerations);
        if (!wireWorld.isEmpty()) animationStarted();
    }

    public World getWorld() {
        return wireWorld;
    }

    public WireWorldView getWireWorldView() {
        return wireWorldView;
    }

    public SimulationThread getSimulationThread() {
        return simulationThread;
    }

    public void animationStarted() {
        if (!wireWorld.isEmpty())
            simulationThread.resumeThread();
    }

    public void animationPaused() {
        try {
            simulationThread.pauseThread();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void animationStopped() {
        try {
            simulationThread.pauseThread();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        wireWorld.clearGrid();
        wireWorldView.updateCellsColor(wireWorld.getGrid());
    }

    public void clickedOpenGeneration() {
        this.animationPaused();
        wireWorldView.showOpenDialog();
    }

    public void openDialogFileChosen(File file){

        wireWorld.clearGrid();
        DataReader dataReader = DataReaderJSON.getInstance();
        wireWorld.setGrid(dataReader.read(file));
        wireWorldView.updateCellsColor(wireWorld.getGrid());
    }

    public void saveDialogFileChosen(File file){
        DataWriter dataWriter = DataWriterJSON.getInstance();
        dataWriter.write(wireWorld.getGrid(),file);
    }

    public void clickedSaveGeneration() {
        this.animationPaused();
        wireWorldView.showSaveDialog();
    }

    public void clickedShowOptionsWindow() {
        animationPaused();
        OptionsView optionsView = new OptionsViewGUI();
        optionsView.setPresenter(this);
        optionsView.open();
    }

    public void clickedExitApplication() {
        System.exit(0);
    }

    public void clickedShowHelpWindow() {
        wireWorldView.showHelp();
    }

    public void clickedShowAboutWindow() {
        wireWorldView.showAbout();
    }
}