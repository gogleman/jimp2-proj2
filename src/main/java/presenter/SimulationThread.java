package presenter;

/**
 * Klasa obsługująca symulację. Tworzy nowy wątek i z zadanym opóźnieniem generuje kolejne stany generacji
 */
public class SimulationThread extends Thread{ //To pozwala na wiele wątków.

    private final Object Monitor = new Object();
    private boolean pauseThreadFlag = false;
    private Presenter presenter;

    /**
     * Opóźnienie animacji w milisekundach
     */
    private int animationSpeed = 1000;

    /**
     * Liczba generacji do stworzenia
     */
    private int numberOfGenerations = 100;

    /**
     * Aktualny numer generacji
     */
    private int currentGenerationNumber;

    public SimulationThread(){
        start();
        try{
            pauseThread();
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    public void run(){

        while (currentGenerationNumber<numberOfGenerations){
            try {
                checkForPaused();
                presenter.getWorld().produceNewWorldState();
                presenter.getWireWorldView().updateCellsColor(presenter.getWorld().getGrid());
                Thread.sleep(animationSpeed);
                currentGenerationNumber++;

            }catch (InterruptedException e){break;}
        }

    }

    private void checkForPaused(){
        synchronized (Monitor){
            while (pauseThreadFlag){
                try{
                    Monitor.wait();
                }catch (Exception ignored){}
            }
        }
    }

    void pauseThread() throws InterruptedException{
        pauseThreadFlag = true;
    }

    void resumeThread(){
        synchronized (Monitor){
            pauseThreadFlag = false;
            Monitor.notify();
        }
    }

    public void setPresenter(Presenter presenter) {
        this.presenter = presenter;
    }

    public void setAnimationSpeed(int animationSpeed) {
        this.animationSpeed = animationSpeed;
    }

    public void setNumberOfGenerations(int numberOfGenerations) {
        this.numberOfGenerations = currentGenerationNumber + numberOfGenerations;
    }

}