package presenter;

import model.World;
import view.OptionsView;
import view.WireWorldView;

import java.io.File;

/**
 * Interfejs prezentera zarządzającego komunikacją pomiędzy obiektami widoku oraz modelu
 */
public interface Presenter{

    /**
     * Ustawia widok głównego okna aplikacji
     * @param wireWorldView Widok okna aplikacji
     */
    void setWireWorldView(WireWorldView wireWorldView);

    /**
     * Ustawia obiekt modelu
     * @param world Obiekt modelu
     */
    void setWorld(World world);

    World getWorld();

    WireWorldView getWireWorldView();

    SimulationThread getSimulationThread();

    /**
     * Reakcja na wciśnięcie przycisku Start
     */
    void animationStarted();

    /**
     * Reakcja na wciśnięcie przycisku Pauza
     */
    void animationPaused();

    /**
     * Reakcja na wciśnięcie przycisku Stop
     */
    void animationStopped();

    /**
     * Reakcja na wybranie z menu pozycji Otwórz generację
     */
    void clickedOpenGeneration();

    /**
     * Reakcja na wybranie pliku generacji do wczytania
     * @param file Plik wejściowy wybrany przez użytkownika
     */
    void openDialogFileChosen(File file);

    /**
     * Reakcja na wybranie z menu pozycji Zapisz generację
     */
    void clickedSaveGeneration();

    /**
     * Reakcja na wybranie stanu generacji do zapisania do pliku
     * @param file Plik wyjściowy wybrany przez użytkownika
     */
    void saveDialogFileChosen(File file);

    /**
     * Reakcja na wybranie z menu pozycji Opcje
     */
    void clickedShowOptionsWindow();

    /**
     * Reakcja na wybranie z menu pozycji Wyjście
     */
    void clickedExitApplication();

    /**
     * Reakcja na wybranie z menu pozycji Otwórz Pomoc
     */
    void clickedShowHelpWindow();

    /**
     * Reakcja na wybranie z menu pozycji O programie
     */
    void clickedShowAboutWindow();

    void setSimulationThread(SimulationThread simulationThread);

    void setWWVGAWPParameters(int numberOfGenerations, int animationSpeed);
}
