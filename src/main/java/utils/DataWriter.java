package utils;

import core.Grid;

import java.io.File;
import java.io.IOException;

/**
 * Interfejs do zapisywania danych generacji do pliku
 */
public interface DataWriter {

    /**
     * Zapisuje dane pobrane z obieku gridu (jego HashMapy) i zapisuje do pliku
     * @param grid Obiekt gridu zawiarający HashMapę z komórkami
     * @param file Plik wyjściowy
     */
    void write (Grid grid, File file);

}
