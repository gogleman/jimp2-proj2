package utils;

import core.Cell;
import core.Coordinate;
import core.Grid;

import java.io.*;
import java.util.Iterator;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class DataWriterJSON implements DataWriter{

    private DataWriterJSON(){
    }

    public static DataWriterJSON getInstance(){
        return DataWriterJSONHolder.instance;
    }

    private static class DataWriterJSONHolder {
        private static final DataWriterJSON instance = new DataWriterJSON();
    }

    //Wszystko będzie zapisywane do postaci WireCell (uniwersalnej), ELECTRONHEAD albo ELECTRONTAIL!
    /**
     * Zapisuje dane pobrane z obieku gridu (jego HashMapy) i zapisuje do pliku
     * @param grid Obiekt gridu zawiarający HashMapę z komórkami
     * @param file Plik wyjściowy
     */
    @SuppressWarnings("unchecked")
    public void write (Grid grid, File file){
        Writer fileWriter = null;

        try{
            JSONObject state = new JSONObject();
            JSONArray stateWire = new JSONArray();
            JSONArray stateHead = new JSONArray();
            JSONArray stateTail = new JSONArray();
            JSONObject stateCoordinates;

            if(grid != null){
                Iterator it = grid.getHashMap().entrySet().iterator();

                while (it.hasNext()) {
                    Map.Entry entry = (Map.Entry) it.next();
                    Coordinate coordinate = (Coordinate) entry.getKey();
                    Cell cell = (Cell) entry.getValue();

                    if (cell.getState() == Cell.State.WIRE) {
                        stateCoordinates = new JSONObject();
                        stateCoordinates.put("x", coordinate.getX());
                        stateCoordinates.put("y", coordinate.getY());
                        stateWire.add(stateCoordinates);

                    } else if (cell.getState() == Cell.State.ELECTRONHEAD) {
                        stateCoordinates = new JSONObject();
                        stateCoordinates.put("x", coordinate.getX());
                        stateCoordinates.put("y", coordinate.getY());
                        stateHead.add(stateCoordinates);

                    } else if (cell.getState() == Cell.State.ELECTRONTAIL) {
                        stateCoordinates = new JSONObject();
                        stateCoordinates.put("x", coordinate.getX());
                        stateCoordinates.put("y", coordinate.getY());
                        stateTail.add(stateCoordinates);

                    }
                }
            }
            state.put("WireCell", stateWire);
            state.put("ElectronHead", stateHead);
            state.put("ElectronTail", stateTail);

            fileWriter = new BufferedWriter(new FileWriter(file));
            fileWriter.write(state.toString());

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileWriter != null) try { fileWriter.close(); } catch (IOException ignore) {}
        }
    }
}