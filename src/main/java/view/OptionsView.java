package view;

import presenter.Presenter;

public interface OptionsView{
    void open();
    void close();
    void setPresenter(Presenter presenter);

}
