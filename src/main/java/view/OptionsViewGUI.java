package view;

import presenter.Presenter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class OptionsViewGUI extends JFrame implements OptionsView, ActionListener {

    private Container container;
    private JPanel buttonsPanel;
    private JPanel textPanel;
    private JButton startButton;
    private Presenter presenter;
    private JTextField numberOfGenerationsField;
    private JTextField animationSpeedField;
    private int numberOfGenerations;
    private int animationSpeed;

    public OptionsViewGUI() {
        super("Options");
        this.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        this.prepareGUI();
    }

    private void prepareGUI() {
        setSize(500, 200);
        buttonsPanel = new JPanel();
        startButton = new JButton("Start");
        buttonsPanel.add(startButton);
        startButton.addActionListener(this);
        textPanel = new JPanel();
        textPanel.setBounds(0, 0, 1000, 600);
        numberOfGenerationsField = new JTextField("Podaj ilosc generacji",25);
        numberOfGenerationsField.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e){
                numberOfGenerationsField.setText("");
            }
        });

        animationSpeedField = new JTextField("Podaj opoznienie miedzy generacjami w ms",25);
        animationSpeedField.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e){
                animationSpeedField.setText("");
            }
        });
        animationSpeedField.addFocusListener(new FocusListener() {
            public void focusGained(FocusEvent e) {
                animationSpeedField.setText("");
            }
            public void focusLost(FocusEvent e) {
            }
        });
        textPanel.add(numberOfGenerationsField);
        textPanel.add(animationSpeedField);
        //CONTAINER:
        container = getContentPane();
        container.add(textPanel, BorderLayout.CENTER);
        container.add(buttonsPanel, BorderLayout.LINE_END);
    }

    public void open() {
        this.setVisible(true);
    }

    public void close() {
        this.dispose();
    }

    public void setPresenter(Presenter presenter) {
        this.presenter = presenter;
    }

    public void actionPerformed(ActionEvent actionEvent) {
        Object source = actionEvent.getSource();
        if (this.startButton.equals(source)) {

            if (numberOfGenerationsField.getText().length() > 0 && numberOfGenerationsField.getText().length() <10)
                numberOfGenerations = Integer.parseInt(numberOfGenerationsField.getText());

            if (animationSpeedField.getText().length() > 0 && animationSpeedField.getText().length() <10)
                animationSpeed = Integer.parseInt(animationSpeedField.getText());
            close();
            presenter.setWWVGAWPParameters(numberOfGenerations, animationSpeed);
        }
    }
}