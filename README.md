# WireWorld
WireWorld is a Java Swing application presenting WireWorld cellular automaton

## Functionalities implemented
* Reading from file cells' startup coordinates 
* Generating next state of automaton
* GUI interface
* Choosing speed of cells' generation
* Setting number of steps to create
* Graphical visualisation in application of automaton state
* Saving cells' state to file

## Technologies used
* Java
* Swing library
* MVP pattern
* JSON format
